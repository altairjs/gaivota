import React from 'react';
import { Col, Row } from 'react-bootstrap';
import Header from '../../components/Header';
import MapItem from '../../components/MapItem';
import FarmSelector from '../../components/FarmSelector';
import {
  GraphicPrecipitation,
  GraphicNdvi,
  GraphicX,
} from '../../components/Graphics';
// import { logout } from "../../auth";

const Home = () => (
  <>
    <Header />
    <Col>
      <Row>
        <Col className="mt-4" lg={9}>
          <MapItem height="80vh" />
        </Col>
        <Col lg={3}>
          <FarmSelector />
        </Col>
      </Row>
      <Col className="text-center">
        <h4 className="mt-2 mb-2">Mixed</h4>
        <GraphicX dataType="231" label="date" />
        <h4 className="mt-2 mb-2">Precipitation only</h4>
        <GraphicPrecipitation />
        <h4 className="mt-2 mb-2">Ndvi only</h4>
        <GraphicNdvi />
      </Col>
    </Col>
  </>
);

export default Home;
