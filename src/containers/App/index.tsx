import React from 'react';
import { BrowserRouter, Switch } from 'react-router-dom';

import Routes from '../../routes';
import ScrollToTop from '../../components/ScrollToTop';

interface Props {
  location: string;
  match: string;
}

const App: React.FC = () => (
  <BrowserRouter>
    <div className="App">
      <ScrollToTop />
      <Switch>
        <Routes />
      </Switch>
    </div>
  </BrowserRouter>
);

export default App;
