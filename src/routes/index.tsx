// eslint-disable-next-line import/no-extraneous-dependencies
import 'react-hot-loader/patch';
import React, { useState, useMemo } from 'react';
import {
  Route, Switch, Redirect,
} from 'react-router-dom';
import Home from '../containers/Home';
import { FarmContext } from '../components/FarmContext';

const Routes = () => {
  const [farm, handleFarm] = useState(null);

  const providerValue = useMemo(() => ({ farm, handleFarm }), [farm, handleFarm]);

  return (
    <FarmContext.Provider value={providerValue}>
      <Switch>
        <Route exact path="/" component={Home} />
        {/* <PrivateRoute exact path="/profile" component={Profile} /> */}
        <Redirect from="*" to="/" />
      </Switch>
    </FarmContext.Provider>
  );
};

export default Routes;
