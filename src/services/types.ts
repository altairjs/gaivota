import { LatLngTuple } from 'leaflet';

export interface GeoJSON {
  type: string;
  crs: Crs;
  features: Feature[];
}

export interface Crs {
  type: string;
  properties: { name: string };
}
export interface Properties {
  g_name: string;
  g_area_ha: string;
  field_id: string;
}

export interface Coordinate {
  coordinate: [number, number];
}

export interface Geometry {
  type: string;
  coordinates: [LatLngTuple[]];
}

export interface Feature {
  type: string;
  properties: Properties;
  geometry: Geometry;
}

export interface CSV {
  farm_id: number;
  name: string;
  latitude: number;
  longitude: number;
  culture: string;
  variety: string;
  total_area: number;
  yield_estimation: number;
  price: number;
}

export interface Option {
  name: string;
  value: string;
}

export interface DataSet {
  label: string;
  backgroundColor: '#5157bb';
  borderColor: '#5157bb';
  borderWidth: 1;
  hoverBackgroundColor: '#5157bb';
  hoverBorderColor: '#5157bb';
  data: number[];
}

export interface Precipitation {
  date: string;
  precipitation_221: number;
  precipitation_231: number;
  precipitation_271: number;
}

export interface Ndvi {
  date: string;
  ndvi_221: string;
  ndvi_231: string;
  ndvi_271: string;
}

export interface Generic{
  label: any
  dataType: any
}
