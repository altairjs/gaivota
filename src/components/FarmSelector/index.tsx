/* eslint-disable @typescript-eslint/camelcase */
import React, { useContext, useState, useEffect } from 'react';
import {
  Card, Form, Col, Button, Row,
} from 'react-bootstrap';
import SelectSearch from 'react-select-search';
import styled from 'styled-components';
import { FarmContext } from '../FarmContext';
import { Farm221Data, Farm231Data, Farm271Data } from '../../data/farms';
import { Option, CSV } from '../../services/types';

import './styles.scss';

const Selector = styled(Card)`
  margin-top: 3vh;
  padding-top: 3vh;
  padding-left: 2vh;
  padding-right: 2vh;
  border-radius: 10px;
`;

const FarmSelector: React.FC = () => {
  const farm = useContext(FarmContext);

  const [options, handleOptions] = useState<Option[]>([
    { name: 'Farm221', value: 'Farm221' },
    { name: 'Farm231', value: 'Farm231' },
    { name: 'Farm271', value: 'Farm271' },
  ]);

  const [information, handleInformation] = useState<CSV>({
    farm_id: 0,
    name: '',
    latitude: 0,
    longitude: 0,
    culture: '',
    variety: '',
    total_area: 0,
    yield_estimation: 0,
    price: 0,
  });

  useEffect(() => {
    const optionsTemp: Option[] = [];
    [Farm221Data, Farm231Data, Farm271Data].map((tempFarm: CSV) => {
      optionsTemp.push({
        name: tempFarm.name,
        value: tempFarm.name,
      });
      return optionsTemp;
    });
    handleOptions(optionsTemp);
    if (farm.farm === null) farm.handleFarm(optionsTemp[0].value);
    switch (farm.farm) {
      default:
      case 'Farm X':
        handleInformation(Farm221Data);
        break;

      case 'Farm Y':
        handleInformation(Farm231Data);
        break;

      case 'Farm Z':
        handleInformation(Farm271Data);
        break;
    }
  }, [farm]);

  return (
    <Selector className="farm-selector">
      <Form.Group>
        <h4>Farms</h4>
        <SelectSearch
          options={options}
          value={farm.farm === null ? options[0].value : farm.farm}
          placeholder="Select Farm..."
          search
          onChange={(event) => farm.handleFarm(String(event))}
        />
      </Form.Group>
      <h4>Information</h4>
      <Col>
        <Form.Group>
          <Form.Row>
            <Form.Label>Farm:</Form.Label>
            <Card.Text>{information?.name}</Card.Text>
          </Form.Row>
        </Form.Group>
        <Form.Group>
          <Form.Row>
            <Form.Label>Culture:</Form.Label>
            <Card.Text>{information?.culture}</Card.Text>
          </Form.Row>
        </Form.Group>
        <Form.Group>
          <Form.Row>
            <Form.Label>Variety:</Form.Label>
            <Card.Text>{information?.variety}</Card.Text>
          </Form.Row>
        </Form.Group>
        <Form.Group>
          <Form.Row>
            <Form.Label>Area:</Form.Label>
            <Card.Text>
              {information?.total_area}
              {' '}
              ha
            </Card.Text>
          </Form.Row>
        </Form.Group>
        <Form.Group>
          <Form.Row>
            <Form.Label>Yeld estimation:</Form.Label>
            <Card.Text>
              {information?.yield_estimation}
              {' '}
              ton/ha
            </Card.Text>
          </Form.Row>
        </Form.Group>
        <Form.Group>
          <Form.Row>
            <Form.Label>Total:</Form.Label>
            <Card.Text>
              {Number(information?.total_area)
                * Number(information?.yield_estimation)}
            </Card.Text>
          </Form.Row>
        </Form.Group>
        <Form.Group>
          <Form.Row>
            <Form.Label>Price:</Form.Label>
            <Card.Text>{information?.price}</Card.Text>
          </Form.Row>
        </Form.Group>
        <Col className="actions">
          <Row>
            <Button className="btn-buy">Buy now</Button>
            <Button className="btn-bid">Bid</Button>
          </Row>
        </Col>
      </Col>
    </Selector>
  );
};

export default FarmSelector;
