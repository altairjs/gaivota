import React, { useState, useEffect } from 'react';
import { Bar } from 'react-chartjs-2';
import { FarmsPrecipitation } from '../../data/farms_precitipation';
import { FarmsNdvi } from '../../data/farms_ndvi';
import { Precipitation, Ndvi, Generic } from '../../services/types';

export const GraphicPrecipitation: React.FC = () => {
  const [labels, handleLabels] = useState<string[]>([]);
  const [datasetX, handleDatasetX] = useState<number[]>([]);
  const [datasetY, handleDatasetY] = useState<number[]>([]);
  const [datasetZ, handleDatasetZ] = useState<number[]>([]);

  /* const data = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
      {
        label: 'My First dataset',
        backgroundColor: '#5157bb',
        borderColor: '#5157bb',
        borderWidth: 1,
        hoverBackgroundColor: '#5157bb',
        hoverBorderColor: '#5157bb',
        data: [65, 59, 80, 81, 56, 55, 40],
      },
    ],
  }; */

  useEffect(() => {
    const tempLabels: string[] = [];
    const tempDatasetX: number[] = [];
    const tempDatasetY: number[] = [];
    const tempDatasetZ: number[] = [];
    FarmsPrecipitation.map((precipitation: Precipitation) => {
      tempLabels.push(precipitation.date);
      tempDatasetX.push(precipitation.precipitation_221);
      tempDatasetY.push(precipitation.precipitation_231);
      tempDatasetZ.push(precipitation.precipitation_271);
      return true;
    });
    handleLabels(tempLabels);
    handleDatasetX(tempDatasetX);
    handleDatasetY(tempDatasetY);
    handleDatasetZ(tempDatasetZ);
  }, []);
  return (
    <Bar
      data={{
        labels,
        datasets: [
          {
            label: 'precipitation_221',
            backgroundColor: '#5157bb',
            borderColor: '#5157bb',
            borderWidth: 1,
            hoverBackgroundColor: '#5157bb',
            hoverBorderColor: '#5157bb',
            data: datasetX,
          },
          {
            label: 'precipitation_231',
            backgroundColor: '#f49c4b',
            borderColor: '#f49c4b',
            borderWidth: 1,
            hoverBackgroundColor: '#f49c4b',
            hoverBorderColor: '#f49c4b',
            data: datasetY,
          },
          {
            label: 'precipitation_271',
            backgroundColor: '#26DEA8',
            borderColor: '#26DEA8',
            borderWidth: 1,
            hoverBackgroundColor: '#26DEA8',
            hoverBorderColor: '#26DEA8',
            data: datasetZ,
          },
        ],
      }}
    />
  );
};

export const GraphicNdvi: React.FC = () => {
  const [labels, handleLabels] = useState<string[]>([]);
  const [datasetX, handleDatasetX] = useState<string[]>([]);
  const [datasetY, handleDatasetY] = useState<string[]>([]);
  const [datasetZ, handleDatasetZ] = useState<string[]>([]);

  useEffect(() => {
    const tempLabels: string[] = [];
    const tempDatasetX: string[] = [];
    const tempDatasetY: string[] = [];
    const tempDatasetZ: string[] = [];
    FarmsNdvi.map((ndvi: Ndvi) => {
      tempLabels.push(ndvi.date);
      tempDatasetX.push(Number(ndvi.ndvi_221.replace(',', '.')).toFixed(12));
      tempDatasetY.push(Number(ndvi.ndvi_231.replace(',', '.')).toFixed(12));
      tempDatasetZ.push(Number(ndvi.ndvi_271.replace(',', '.')).toFixed(12));
      return true;
    });
    handleLabels(tempLabels);
    handleDatasetX(tempDatasetX);
    handleDatasetY(tempDatasetY);
    handleDatasetZ(tempDatasetZ);
  }, []);
  return (
    <Bar
      data={{
        labels,
        datasets: [
          {
            label: 'ndvi_221',
            backgroundColor: '#5157bb',
            borderColor: '#5157bb',
            borderWidth: 1,
            hoverBackgroundColor: '#5157bb',
            hoverBorderColor: '#5157bb',
            data: datasetX,
          },
          {
            label: 'ndvi_231',
            backgroundColor: '#f49c4b',
            borderColor: '#f49c4b',
            borderWidth: 1,
            hoverBackgroundColor: '#f49c4b',
            hoverBorderColor: '#f49c4b',
            data: datasetY,
          },
          {
            label: 'ndvi_271',
            backgroundColor: '#26DEA8',
            borderColor: '#26DEA8',
            borderWidth: 1,
            hoverBackgroundColor: '#26DEA8',
            hoverBorderColor: '#26DEA8',
            data: datasetZ,
          },
        ],
      }}
    />
  );
};

export const GraphicX: React.FC<Generic> = (props: Generic) => {
  // const { label, dataType } = props;

  const [labels, handleLabels] = useState<string[]>([]);
  const [datasetPrecipitation, handleDatasetPrecipitation] = useState<number[]>([]);
  const [datasetNdvi, handleDatasetNdvi] = useState<string[]>([]);

  useEffect(() => {
    const tempLabels: string[] = [];
    const tempDatasetPrecipitation: number[] = [];
    const tempDatasetNdvi: string[] = [];
    FarmsNdvi.map((ndvi: Ndvi) => {
      tempLabels.push(ndvi.date);
      tempDatasetNdvi.push(Number(ndvi.ndvi_221.replace(',', '.')).toFixed(12));
      return true;
    });
    FarmsPrecipitation.map((precipitation: Precipitation) => {
      tempDatasetPrecipitation.push(precipitation.precipitation_221);
      return true;
    });
    handleLabels(tempLabels);
    handleDatasetNdvi(tempDatasetNdvi);
    handleDatasetPrecipitation(tempDatasetPrecipitation);
  }, []);
  return (
    <Bar
      data={{
        labels,
        datasets: [
          {
            label: 'ndvi_221',
            fill: false,
            backgroundColor: '#5157bb',
            borderColor: '#5157bb',
            borderWidth: 1,
            hoverBackgroundColor: '#5157bb',
            hoverBorderColor: '#5157bb',
            data: datasetNdvi,
          },
          {
            label: 'precipitation_221',
            type: 'line',
            fill: false,
            backgroundColor: '#f49c4b',
            borderColor: '#f49c4b',
            borderWidth: 1,
            hoverBackgroundColor: '#f49c4b',
            hoverBorderColor: '#f49c4b',
            data: datasetPrecipitation,
          },
        ],
      }}
    />
  );
};

export default GraphicPrecipitation;
