import { createContext } from 'react';

export const FarmContext = createContext<{ farm: any; handleFarm: any }>({
  farm: '',
  handleFarm: () => false,
});
