import { useEffect } from "react";
import { withRouter } from "react-router-dom";

const ScrollToTop = ({ history }: any) => {
  useEffect(() => {
    const historyListen = history.listen(() => {
      window.scrollTo(0, 0);
    });
    return () => {
      historyListen();
    };
  }, [history]);

  return null;
};

export default withRouter(ScrollToTop);
