import React from 'react';

interface FontAwesome {
  type: string;
  name: string;
}

const FontAwesome = ({ type, name }: FontAwesome) => (
  <i className={`${type} fa-${name}`} />
);

export default FontAwesome;
