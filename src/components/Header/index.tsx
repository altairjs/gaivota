import React from 'react';

import './styles.scss';

import { Navbar, Nav, NavDropdown } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import Logo from '../../images/logo.png';
// import FontAwesome from '../FontAwesome';
// import { removeSpecialChars } from '../../services/mask';

const ConstructorImage = styled.div`
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  border-radius: 50%;
  border: 1px solid #8a98ba;
  width: 50px;
  height: 50px;
  align-items: center;
  text-align: center;
  display: flex;
`;

const Header: React.FC = () => {
  const checkActive = (path: string) => window.location.pathname === `/${path}`;

  return (
    <Navbar className="nav-bar" expand="lg">
      <Navbar.Brand href={`${process.env.PUBLIC_URL}/`}>
        <img src={Logo} alt="Logo" />
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link
            active={checkActive('')}
            as={Link}
            to={`${process.env.PUBLIC_URL}/`}
          >
            Farm xxx
          </Nav.Link>
        </Nav>
        <>
          <ConstructorImage style={{ backgroundImage: `url(${Logo})` }} />
          <NavDropdown
            title="Hello, country_region_state_city"
            id="collapsible-nav-dropdown"
          >
            <NavDropdown.Item
              as={Link}
              to={`${process.env.PUBLIC_URL}/perfil/configs`}
            >
              Configs
            </NavDropdown.Item>
            <NavDropdown.Item
              as={Link}
              to={`${process.env.PUBLIC_URL}/perfil/pedidos`}
            >
              Profile
            </NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item>Logout</NavDropdown.Item>
          </NavDropdown>
        </>
        <Nav.Link
          active={checkActive('login')}
          as={Link}
          to={`${process.env.PUBLIC_URL}/login`}
        >
          Login
        </Nav.Link>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default Header;
