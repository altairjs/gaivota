/* eslint-disable max-len */
import React, { useState, useContext, useEffect } from 'react';
import {
  Map, Marker, Popup, TileLayer,
} from 'react-leaflet';
import { LatLngTuple } from 'leaflet';
import 'leaflet/dist/leaflet.css';
import { Feature, GeoJSON } from '../../services/types';

import {
  Farm221,
  Farm221Data,
  Farm231,
  Farm231Data,
  Farm271,
  Farm271Data,
} from '../../data/farms';
import { FarmContext } from '../FarmContext';

interface Props {
  height: string;
}

const MapItem: React.FC<Props> = (props: Props) => {
  const { height } = props;

  const farm = useContext(FarmContext);

  const [farmOrigin, handleFarmOrigin] = useState<{
    latitude: number;
    longitude: number;
  }>({ latitude: Farm221Data.latitude, longitude: Farm221Data.longitude });

  /* const [activeFarm, handleActiveFarm] = useState<Feature>({
    type: '',
    geometry: { type: '', coordinates: [[]] },
    // eslint-disable-next-line @typescript-eslint/camelcase
    properties: { field_id: '', g_area_ha: '', g_name: '' },
  }); */

  const selectedFarm = () => {
    let farmSelected: GeoJSON = {
      type: '',
      crs: { properties: { name: '' }, type: ' ' },
      features: [],
    };

    switch (farm.farm) {
      default:
      case 'Farm X':
        farmSelected = Farm221;
        return farmSelected;

      case 'Farm Y':
        farmSelected = Farm231;
        return farmSelected;

      case 'Farm Z':
        farmSelected = Farm271;
        return farmSelected;
    }
  };

  useEffect(() => {
    switch (farm.farm) {
      default:
      case 'Farm X':
        handleFarmOrigin({
          latitude: Farm221Data.latitude,
          longitude: Farm221Data.longitude,
        });
        break;

      case 'Farm Y':
        handleFarmOrigin({
          latitude: Farm231Data.latitude,
          longitude: Farm231Data.longitude,
        });
        break;

      case 'Farm Z':
        handleFarmOrigin({
          latitude: Farm271Data.latitude,
          longitude: Farm271Data.longitude,
        });
        break;
    }
  }, [farm.farm]);

  // eslint-disable-next-line no-lone-blocks
  {
    /* const converter = JSON.parse(`{
     "type": "FeatureCollection",
     "crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
     "features": [
     { "type": "Feature", "properties": { "g_name": "Talhão-0", "g_area_ha": "18.32915032824457", "field_id": "1515" }, "geometry": { "type": "Polygon", "coordinates": [ [ [ -74.208312, 4.685498 ], [ -74.204471, 4.685476 ], [ -74.203934, 4.681862 ], [ -74.208419, 4.681926 ], [ -74.208312, 4.685498 ] ] ] } },
     { "type": "Feature", "properties": { "g_name": "Talhão-0", "g_area_ha": "4.154656196260981", "field_id": "1314" }, "geometry": { "type": "Polygon", "coordinates": [ [ [ -74.211537, 4.690747 ], [ -74.208819, 4.690709 ], [ -74.208802, 4.690576 ], [ -74.208759, 4.689474 ], [ -74.211531, 4.689517 ], [ -74.211537, 4.690747 ] ] ] } },
     { "type": "Feature", "properties": { "g_name": "Talhão-0", "g_area_ha": "18.10678373520952", "field_id": "1315" }, "geometry": { "type": "Polygon", "coordinates": [ [ [ -74.212951, 4.685609 ], [ -74.209111, 4.685587 ], [ -74.209133, 4.683641 ], [ -74.208489, 4.683641 ], [ -74.208489, 4.68193 ], [ -74.212479, 4.682059 ], [ -74.213359, 4.684347 ], [ -74.212951, 4.684497 ], [ -74.212951, 4.685609 ] ] ] } },
     { "type": "Feature", "properties": { "g_name": "Talhão-0", "g_area_ha": "37.04083596623678", "field_id": "1313" }, "geometry": { "type": "Polygon", "coordinates": [ [ [ -74.208167, 4.689308 ], [ -74.208296, 4.685673 ], [ -74.217048, 4.685951 ], [ -74.216619, 4.68933 ], [ -74.208167, 4.689308 ] ] ] } }
     ]
     }
     `);
   <GeoJSON key="1" data={converter} />
   */
  }

  return (
    <Map
      center={[farmOrigin.latitude, farmOrigin.longitude]}
      zoom={13}
      style={{ width: '100%', height: `${height}` }}
    >
      <TileLayer
        attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {selectedFarm().features.map((feature: Feature, index: number) => (
        <Marker
          // eslint-disable-next-line react/no-array-index-key
          key={`${feature.type}_${index}`}
          position={
            feature.geometry.coordinates[0].map(
              (latLongTuple: LatLngTuple) => latLongTuple,
            )[0]
          }
          attribution={feature.properties.g_name}
          title={feature.properties.g_name}
          /* onClick={() => {
            handleActiveFarm(feature);
          }} */
        >
          {console.log(`Teste: ${farm.farm}`, feature.geometry.coordinates[0].map(
            (latLongTuple: LatLngTuple) => [latLongTuple[1], latLongTuple[0]],
          ))}
          <Popup>{`${feature.properties.g_name}/${farm.farm}`}</Popup>
        </Marker>
      ))}
    </Map>
  );
};

export default MapItem;
